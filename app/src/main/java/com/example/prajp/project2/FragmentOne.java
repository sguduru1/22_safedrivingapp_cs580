package com.example.prajp.project2;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import com.example.prajp.project2.database.DatabaseOperations;
/**
 * Created by prajp on 4/11/2017.
 */

public class FragmentOne extends Fragment {

    //Button button;
    TextView message;
    Button buttonON,buttonOFF,saveMessage1;
    String custMessage,status;
    android.content.Context ctx = this.getActivity();
    int flag = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_one_layout,container,false);


        buttonON = (Button)v.findViewById(R.id.buttonON);
        buttonOFF = (Button)v.findViewById(R.id.buttonOFF);


        buttonON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity().getBaseContext(),"You clicked ON",Toast.LENGTH_LONG).show();
                status = "ON";
                flag = 1;//send msg
            }
        });
        buttonOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "OFF";
                flag = 0;//do not send
            }
        });

        message = (TextView) v.findViewById(R.id.editTextNew);
        message.setText("I am driving currently. Will call you soon.");
      //  saveMessage1 = (Button)v.findViewById(R.id.saveMessage);
       // Toast.makeText(getActivity().getBaseContext(),"hii",Toast.LENGTH_LONG).show();
       // saveMessage1.setOnClickListener(this);
       /*saveMessage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // message = (EditText) v.findViewById(R.id.editText2);
                Toast.makeText(getActivity().getBaseContext(),"prajakta",Toast.LENGTH_LONG).show();
                custMessage = message.getText().toString();
                Toast.makeText(getActivity().getBaseContext(),custMessage,Toast.LENGTH_LONG).show();
                DatabaseOperations db = new DatabaseOperations(ctx);
                db.insertInfo(db,status,custMessage);
                Toast.makeText(getActivity().getBaseContext(),"saved message successfully",Toast.LENGTH_LONG).show();
            }
        });*/
        return v;
    }

    public void saveModifiedMessage(View v){
        Toast.makeText(getActivity().getBaseContext(),"hii11",Toast.LENGTH_LONG).show();
        custMessage = message.getText().toString();
        Toast.makeText(getActivity().getBaseContext(),custMessage,Toast.LENGTH_LONG).show();
        DatabaseOperations db = new DatabaseOperations(ctx);
        db.insertInfo(db,status,custMessage);
        Toast.makeText(getActivity().getBaseContext(),"saved message successfully",Toast.LENGTH_LONG).show();
    }


}
