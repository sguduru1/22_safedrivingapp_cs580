package com.example.prajp.project2.speedcalculation;

/**
 * Created by prajp on 5/7/2017.
 */

public class SensorData {

    private double velocity = 0;
    private long time = 0;
    private double acceleration = 0;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }



}
