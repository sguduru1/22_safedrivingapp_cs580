package com.example.prajp.project2.speedcalculation;

/**
 * Created by prajp on 5/8/2017.
 */

public class AccelerationData {

    private float x = 0;
    private float y = 0;
    private float z = 0;
    private int cnt = 1;

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getCnt() {
        return cnt;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public void setX(float x) {

        this.x = x;
    }

    public float getZ() {
        return z;
    }

    public AccelerationData(float x, float y, float z, int cnt) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.cnt = cnt;
    }


    public float getForce(){
        return getX()*getX()+getY()*getY()+getZ()*getZ();
    }
}
