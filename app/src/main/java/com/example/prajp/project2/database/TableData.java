package com.example.prajp.project2.database;

import android.provider.BaseColumns;

/**
 * Created by prajp on 4/12/2017.
 */

public class TableData {

    public TableData(){

    }

    public static abstract class TableInfo implements BaseColumns{
        public static final String autoReplyStatus = "replyStatus";
        public static final String message = "message";
        public static final String database_name = "app_info";
        public static final String table_name = "appTable";
    }
}
