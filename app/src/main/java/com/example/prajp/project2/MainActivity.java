package com.example.prajp.project2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.telephony.SmsManager;
import android.widget.Toast;
import android.view.MotionEvent;
import android.widget.TextView;
import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;
import android.hardware.Sensor;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.R.attr.*;
import java.util.List;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.app.NotificationManager;
import android.annotation.TargetApi;
import android.os.Build;
import android.content.Intent;
import com.example.prajp.project2.speedcalculation.AccelerationData;
import java.util.LinkedList;

import com.example.prajp.project2.speedcalculation.SensorData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    android.content.Context ctx = this;
    SensorManager sensorManager;
    Sensor accelerometer;
    BroadcastReceiver receiver;

    NotificationManager mNotificationManager;
    long curTime = System.currentTimeMillis();
    private long lastUpdate = 0;// curTime;
    private float last_x, last_y, last_z;
    AccelerationData point = new AccelerationData(0,0,0,0);
    LinkedList<AccelerationData> accData = new LinkedList<AccelerationData>();
    List<SensorData> dataList = new ArrayList<SensorData>();
    TextView myTextView;
    TextView playButton;

    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null) {

            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
            sensorManager.registerListener(this,accelerometer,sensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Toast.makeText(this, "No sensor available", Toast.LENGTH_SHORT).show();
        }

        myTextView = (TextView)findViewById(R.id.textViewdata);
       // playButton = (TextView) findViewById(R.id.bt3);
       // playButton.setOnTouchListener(this);


    }

   /* @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        sendMessage(v);
        return true;
    }*/

    @Override
    public void onSensorChanged(SensorEvent event) {


        SensorData sensorData = new SensorData();
        float xA=0,yA=0,zA=0;
        double totalAcceleration = 0;
        float totalVal = 0;
        double initial_velocity = 0;
        if (0<dataList.size())
            initial_velocity = dataList.get(dataList.size()-1).getVelocity();
        double final_velocity = initial_velocity;
        Sensor sensor = event.sensor;
        if (sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            xA = event.values[0];
            yA = event.values[1];
            zA = event.values[2];
        }
        long curTime = System.currentTimeMillis();


        if ( (curTime - lastUpdate) > 15000) {
            long diffTime = (curTime - lastUpdate);
            totalVal = (xA*xA + yA*yA + zA*zA);
            totalAcceleration = Math.sqrt(totalVal);

            final_velocity = initial_velocity + ( totalAcceleration * (diffTime / 1000));

            sensorData.setAcceleration(totalAcceleration);
            sensorData.setTime(curTime);
            sensorData.setVelocity(final_velocity);
            dataList.add(sensorData);
            last_x = -xA;
            last_y = -yA;
            last_z = -zA;
            initial_velocity = final_velocity;
            if (15 < final_velocity )
            {
                Intent intent = new Intent("ABOVE_SPEED_LIMIT");
                intent.putExtra("SPEED", final_velocity);
                sendBroadcast(intent);
            }
            else
            {
                Intent intent = new Intent("BELOW_SPEED_LIMIT");
                intent.putExtra("SPEED", final_velocity);
                sendBroadcast(intent);

            }
        }

        if ((curTime - lastUpdate) >= 10500) {
            myTextView.setText(String.format("%.2f",final_velocity));
        }
        if(lastUpdate == 0 ||(curTime - lastUpdate ) > 15000){
            lastUpdate = curTime;
        }
    }

    // Mean of the squared values
    /*public AccelerationData getAveragePoint(){

        float x =0, y= 0,z=0 , cnt = 0;
        for(AccelerationData current : accData)
        {
            x+= current.getX();
            y+= current.getY();
            z+= current.getZ();
            cnt+= current.getCnt();
        }
        AccelerationData avgData =  new AccelerationData(x/cnt,y/cnt,z/cnt,0);
        return avgData;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float x=0,y=0,z=0;
        // Toast.makeText(this,"value chnaged",Toast.LENGTH_SHORT).show();
        Sensor sensor = event.sensor;
        if (sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
        }

        point.setX(point.getX()+x);
        point.setY(point.getY()+y);
        point.setZ(point.getZ()+z);

        point.setCnt(point.getCnt()+1);

        if(point.getCnt() == 1){
            accData.add(point);
            point = new AccelerationData(0,0,0,0);
        }

        SensorData sensorData = new SensorData();

        double totalAcceleration = 0;
        float totalVal = 0;
        double initial_velocity = 0;
        if (0<dataList.size())
            initial_velocity = dataList.get(dataList.size()-1).getVelocity();
        double final_velocity = initial_velocity;

        long curTime = System.currentTimeMillis();
//        long curTime = event.timestamp;


        if ( (curTime - lastUpdate) > 5000 && accData.size() > 0) {
            long diffTime = (curTime - lastUpdate);
            // lastUpdate = curTime;
            AccelerationData averageData;
            averageData = getAveragePoint();
            if (0 < averageData.getForce())
                totalAcceleration = (averageData.getX()+averageData.getY()+averageData.getZ())/Math.sqrt(averageData.getForce());

            *//*totalVal = (x*x + y*y + z*z);
            totalAcceleration = Math.sqrt(totalVal);*//*

            final_velocity = initial_velocity + ( totalAcceleration * (diffTime / 1000));

            sensorData.setAcceleration(totalAcceleration);
            sensorData.setTime(curTime);
            sensorData.setVelocity(final_velocity);
            dataList.add(sensorData);
            *//*last_x = xA;
            last_y = yA;
            last_z = zA;*//*
            initial_velocity = final_velocity;
            accData = new LinkedList<AccelerationData>();
        }
        if ((curTime - lastUpdate) >= 4500) {
            myTextView.setText("Final velocity: " + final_velocity);
        }
        if(lastUpdate == 0 ||(curTime - lastUpdate ) > 5000){
            lastUpdate = curTime;
        }
    }*/

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }



    public void multiple(View v){
        Intent intent  = new Intent(getApplicationContext(),MultiFragmentActivity.class);
        startActivity(intent);
    }
    /*public void sendMessage(View v){
        List<String> numberList = new ArrayList<String>();
        numberList.add("6072324445");
        numberList.add("6072970980");
        try {

            SmsManager smsManager = SmsManager.getDefault();
            for(String number : numberList) {
                smsManager.sendTextMessage(number, null, "I am driving currently. Will call you soon.", null, null);
            }
           // Toast.makeText(getApplicationContext(), "SMS Sent!",
             //       Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS failed, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }*/

}
