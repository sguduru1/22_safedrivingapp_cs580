package com.example.prajp.project2.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.example.prajp.project2.MainActivity;

import android.telephony.PhoneStateListener;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.widget.Toast;
import android.app.Activity;
/**
 * Created by prajp on 5/7/2017.
 */

public class IncomingCallReceiver extends BroadcastReceiver {

    MainActivity activity;
    static boolean blockActivated = false;
    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    public IncomingCallReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ITelephony telephonyService;
       // MainActivity activity = context.getApplicationContext().;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String stateRinging = intent.getStringExtra(TelephonyManager.EXTRA_STATE_RINGING);
            //Toast.makeText(context,"Ringing State Number is -"+state,Toast.LENGTH_SHORT).show();
            String action = intent.getAction();
           // Toast.makeText(context,"SPEED -"+intent.getExtras().getString("SPEED"),Toast.LENGTH_SHORT).show();
           // Toast.makeText(context,"ACTION -"+action,Toast.LENGTH_SHORT).show();

            if (action.equals("ABOVE_SPEED_LIMIT"))
            {
                blockActivated = true;
            }
            else if (action.equals("BELOW_SPEED_LIMIT"))
            {
                blockActivated = false;
            }
            // if(state.equals(stateRinging))
            //{
                String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                //intent.getStringExtra(intent.ACTION_NEW_OUTGOING_CALL);
            if(null != incomingNumber && !state.equals(TelephonyManager.EXTRA_STATE_IDLE)
                    && !state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK) && blockActivated)
            {

                Class c = Class.forName(telephony.getClass().getName());
                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                telephonyService = (ITelephony)m.invoke(telephony);
                telephonyService.endCall();
                //Toast.makeText(context,"Ringing State Number is -"+incomingNumber,Toast.LENGTH_SHORT).show();
                sendMessage(context,incomingNumber);
            }




            /*
            if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){

            }
            if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))){
                Toast.makeText(context,"Received State",Toast.LENGTH_SHORT).show();
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                Toast.makeText(context,"Idle State",Toast.LENGTH_SHORT).show();
            }
            */

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void sendMessage(Context context,String incomingNumber){
        List<String> numberList = new ArrayList<String>();
       // numberList.add("6072324445");
       // numberList.add("6072970980");
        try {

            SmsManager smsManager = SmsManager.getDefault();
           // for(String number : numberList) {
                smsManager.sendTextMessage(incomingNumber, null, "I am driving currently. Will call you soon.", null, null);
            //}
            // Toast.makeText(getApplicationContext(), "SMS Sent!",
            //       Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(context,
                    "SMS failed, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


}