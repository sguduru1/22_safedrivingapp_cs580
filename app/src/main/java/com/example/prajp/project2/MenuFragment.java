package com.example.prajp.project2;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by prajp on 4/11/2017.
 */

public class MenuFragment extends Fragment {

    Fragment frag;
    FragmentTransaction fragmentTransaction;
    public MenuFragment(){

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.menu_multi_fragment,container,false);

        frag = new FragmentOne();
        fragmentTransaction = getFragmentManager().beginTransaction().add(R.id.container,frag);
        fragmentTransaction.commit();
        Button b1 = (Button) v.findViewById(R.id.buttonFrag1);
        Button b2 = (Button) v.findViewById(R.id.buttonFrag2);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                frag = new FragmentOne();
                fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.container,frag);
                fragmentTransaction.commit();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                frag = new FragmentTwo();
                fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.container,frag);
                fragmentTransaction.commit();
            }
        });
        return v;
    }
}
