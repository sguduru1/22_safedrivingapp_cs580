package com.example.prajp.project2.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.prajp.project2.database.TableData;
/**
 * Created by prajp on 4/12/2017.
 */

public class DatabaseOperations extends SQLiteOpenHelper {

    public static final int databaseVersion = 1;
    public String CREATE_QUERY = "CREATE TABLE"+ TableData.TableInfo.table_name+"("+
            TableData.TableInfo.autoReplyStatus +
            " TEXT,"+ TableData.TableInfo.message+" TEXT );";

    public DatabaseOperations(android.content.Context context){
        super(context, TableData.TableInfo.database_name,null,databaseVersion);
        Log.d("Database Operations","DB created");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
        Log.d("Database Operations","Table created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertInfo(DatabaseOperations dob,String status,String msg){
        SQLiteDatabase SQ =  dob.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableData.TableInfo.autoReplyStatus,status);
        cv.put(TableData.TableInfo.message,msg);
        long k = SQ.insert(TableData.TableInfo.table_name,null,cv);
        Log.d("DB op","1 row inserted");
        SQ.close();
    }
}
